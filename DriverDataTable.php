<?php

namespace App\DataTables;

use App\Models\CarRental\Driver;
use Yajra\DataTables\Services\DataTable;

class DriverDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->filter(function ($query) {
                if (request('firstname')) {
                    $query->where('firstname', 'like', request('firstname').'%');
                }
                if (request('lastname')) {
                    $query->where('lastname', 'like', request('lastname').'%');
                }
                if (request('residence_city')) {
                    $query->where('residence_city', 'like', request('residence_city').'%');
                }
            })
            ->addIndexColumn()
            ->addColumn('action', 'admin.car_rental.driver.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Driver $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '80px', 'title' => 'Action'], true)
            ->minifiedAjax()
            ->parameters([
                $this->getBuilderParameters(),
                'pageLength' => 25,
            ]);

    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'firstname',
            'lastname',
            'residence_city',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Driver_' . date('YmdHis');
    }
}
