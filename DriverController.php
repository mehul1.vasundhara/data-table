<?php

namespace App\Http\Controllers\Admin\CarRental;

use App\DataTables\DriverDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\DriverUpdateRequest;
use App\Http\Requests\DriverCreateRequest;
use App\Models\CarRental\Driver;
use App\Repositories\CarRental\DriverRepository;
use Illuminate\Http\Request;

class DriverController extends Controller
{

    protected $driverRepository;

    public function __construct(DriverRepository $driverRepository)
    {
        $this->driverRepository = $driverRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DriverDataTable $dataTable)
    {
        return $dataTable->render('admin.car_rental.driver.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.car_rental.driver.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DriverCreateRequest $request)
    {
        $input = $request->all();
        $this->driverRepository->create($input);

        return redirect()->route('admin.car_rental.drivers.index')
            ->with('success', __('notifications.created_successfully', ['resource' => 'Driver']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        return view('admin.car_rental.driver.show', compact('driver'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        return view('admin.car_rental.driver.edit', compact('driver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DriverUpdateRequest $request, $id)
    {
        $input = $request->all();
        $this->driverRepository->update($input, $id);

        return redirect()->route('admin.car_rental.drivers.index')
            ->with('success', __('notifications.created_successfully', ['resource' => 'Driver']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
