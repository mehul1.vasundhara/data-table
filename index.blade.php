@extends('layouts.page')

@section('title', __('car_rental/driver.title'))

@section('custom-nav-item-manu')
    <link href="{{ asset('vendor/searching-bar/searching-menu.css')}}" rel="stylesheet">

    <li class="nav-item  user-menu">
        <a class="nav-link " href="#searchSidebar" data-toggle="collapse" data-target="#searchSidebar"
           aria-expanded="false" aria-controls="searchSidebar">
            <span class="fa fa-search">
            </span>
        </a>
    </li>
    <aside class="main-sidebar sidebar-dark-primary elevation-custom collapse" id="searchSidebar">
        <div class="sidebar">
            <nav class="mt-2">
                {!! Form::model(request()->all(), ['id' => 'search-form', 'class' => 'mb-5','autocomplete'=>'off', 'method' => 'post']) !!}
                <ul class="nav nav-pills nav-sidebar flex-column ">
                    <li class="nav-item has-treeview text-right text-white">
                        <i class="fas fa-times-circle right" href="#searchSidebar" data-toggle="collapse"
                           data-target="#searchSidebar" aria-expanded="false" aria-controls="searchSidebar"></i>
                        <hr>
                    </li>
                    <li class="nav-item fild-display  text-white">
                        <div class="first"><label>First name</label></div>
                        <div class="form-group">
                            {!! Form::text('firstname', null,
                            [
                                'id' => 'firstname',
                                'class' => 'form-control', 'style' => 'width:100%',
                            ])
                        !!}
                        </div>
                    </li>

                    <li class="nav-item fild-display  text-white">
                        <div class="first"><label>Last name</label></div>
                        <div class="form-group">
                            {!! Form::text('lastname', null,
                            [
                                'id' => 'lastname',
                                'class' => 'form-control', 'style' => 'width:100%',
                            ])
                        !!}
                        </div>
                    </li>

                    <li class="nav-item fild-display  text-white">
                        <div class="first"><label>Residence city</label></div>
                        <div class="form-group">
                            {!! Form::text('residence_city', null,
                            [
                                'id' => 'residence_city',
                                'class' => 'form-control', 'style' => 'width:100%',
                            ])
                        !!}
                        </div>
                    </li>

                    <li class="nav-item fild-display text-white ">
                        <hr>
                        <button class="btn btn-primary d-block" id="search">Search <i class="fa fa-search fa-sm"></i></button>
                    </li>
                    <li class="nav-item fild-display text-white ">
                        <br>
                        <button class="btn btn-info d-block" id="reset-btn">Clear </button>
                    </li>
                </ul>
                {{ Form::close() }}
            </nav>
        </div>
    </aside>
@endsection

@section('content')
    @parent
    @component('components.box')
        @slot('title')
            @lang('car_rental/driver.title')
        @endslot
        @slot('actions')
            <a href="{{ route('admin.car_rental.drivers.create') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i>
                @lang('common.add_new')
            </a>
        @endslot
        <div class="table-responsive">
{{--            {!! $dataTable->table() !!}--}}
            <table id="data-table" class="table">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>#</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Residence city</th>
                </tr>
                </thead>
            </table>
        </div>
    @endcomponent
@endsection

{{--@section('js')--}}
{{--    @parent--}}
{{--    {!! $dataTable->scripts() !!}--}}
{{--@endsection--}}

@push('js')
    <script>
        $(function () {
            getData();

            $('#search-form').submit(function( event ) {
                event.preventDefault();
                var queryString = $('#search-form').serialize();
                $("#data-table").dataTable().fnDestroy();
                getData(queryString);
            });
            //Initialize Select2 Elements
            $('.select2').select2();
        });

        function getData(querystring = ''){
            $('#data-table').DataTable({
                serverSide: true,
                pageLength: 25,
                responsive : false,
                autoWidth : false,
                searching : false,
                processing: true,
                ajax: "{{ route('admin.car_rental.drivers.index') }}?"+querystring,
                columns: [
                    { data: 'action', name: 'action', orderable: false, title: 'Action', searchable: false  },
                    {
                        data: 'DT_Row_Index',
                        name: 'DT_Row_Index',
                        title: '#',
                        orderable: false,
                        searchable: false
                    },
                    { data: 'firstname' },
                    { data: 'lastname' },
                    { data: 'residence_city' },
                ],
                "language":{
                    {{--                    "url":"{{asset('vendor/datatable/plugins/i18n/'.app()->getLocale().'.json')}}"--}}
                }
            })
        }

        $('#reset-btn').click(function(){
            $('#search-form')[0].reset();
            $('#maintenance_type, #maintenance_activity').val('').trigger('change');
            // $("#data-table").dataTable().fnDestroy();
            // getData();
        });

    </script>
@endpush
